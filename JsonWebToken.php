<?php

namespace JWT;

use Firebase\JWT\JWT;
use yii\base\Object;

/**
 * Class JWT
 * @package JWT
 */
class JsonWebToken extends Object
{
    /**
     * @var string
     */
    public $jwtKey;
    /**
     * @var string
     */
    public $algorithm = 'HS512';

    /**
     * @param $jwt
     * @throws \Firebase\JWT\BeforeValidException
     * @throws \Firebase\JWT\ExpiredException
     * @throws \Firebase\JWT\SignatureInvalidException
     * @throws \UnexpectedValueException
     * @return object
     */
    public function decode($jwt)
    {
        return JWT::decode($jwt, $this->jwtKey, $this->algorithm);
    }

    /**
     * @param array $data
     * @return string
     */
    public function encode(array $data)
    {
        return JWT::encode($data, $this->jwtKey, $this->algorithm);
    }
}