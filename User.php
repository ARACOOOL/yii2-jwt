<?php

namespace JWT;

use yii\web\IdentityInterface;

/**
 * Class User
 * @package JWT
 */
class User extends \yii\web\User
{
    /**
     * @param string $jwt
     * @return IdentityInterface
     */
    public function loginByJWT(string $jwt)
    {
        \Yii::$app->jwt->decode($jwt);
        return $this->loginByAccessToken($jwt);
    }
}