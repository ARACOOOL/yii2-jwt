# yii2-jwt

JWT implementation for Yii2 Authorization process

For details see [JWT official website](https://jwt.io/introduction/).

## Installation

To install (only master is available now) run:

```
    composer require aracoool/yii2-jwt
```

## Usage

Configured components

```php
...
'jwt' => [
    'class' => JsonWebToken::class,
    'jwtKey' => 'key_for_signature'
],
'user' => [
    'class' => \JWT\User::class,
    'identityClass' => \app\models\User::class,
    'enableAutoLogin' => true,
],
...
```

# Usage of component

Encode data

```php
\Yii::$app->jwt->encode([
    'name' => 'Username'
]);
```
Decode hash

```php
\Yii::$app->jwt->decode('jwt.hash.string');
```

# Configure controller

```php
...
'authenticator' => [
    'class' => \JWT\HttpJwtAuth::class,
    'except' => ['login', 'error']
]
...
```