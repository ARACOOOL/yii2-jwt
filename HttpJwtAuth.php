<?php

namespace JWT;

use yii\filters\auth\AuthMethod;
use yii\web\IdentityInterface;
use yii\web\Request;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

/**
 * Class Jwt
 */
class HttpJwtAuth extends AuthMethod
{
    /**
     * Authenticates the current user.
     * @param User $user
     * @param Request $request
     * @param Response $response
     * @return IdentityInterface the authenticated user identity. If authentication information is not provided, null will be returned.
     * @throws \UnexpectedValueException
     * @throws \Firebase\JWT\SignatureInvalidException
     * @throws \Firebase\JWT\ExpiredException
     * @throws \Firebase\JWT\BeforeValidException
     * @throws UnauthorizedHttpException if authentication information is provided but is invalid.
     */
    public function authenticate($user, $request, $response)
    {
        $authHeader = $request->getHeaders()->get('Authorization');
        if (!$authHeader) {
            return null;
        }

        [$jwt] = sscanf($authHeader, 'Bearer %s');
        if (!$jwt) {
            return null;
        }

        try {
            $identity = $user->loginByJWT($jwt);
        } catch (\Exception $exception) {
            \Yii::error($exception->getMessage());
            $this->handleFailure($response);
        }

        return $identity;
    }
}